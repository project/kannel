<?php
/**
 * @file
 * Settings page callback file for the counter module.
 */

/**
 * Menu callback;
 */
 
function kannel_admin_basic() {
  $form = array();
  // only administrators can access this function
  
  // Generate the form - settings applying to all patterns first
  $form['kannel_basic'] = array(
    '#type' => 'fieldset',
    '#weight' => -100,
    '#title' => t('Basic settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE    
  );
  
  $form['kannel_basic']['kannel_basic_host'] = array(
    '#type' => 'textfield',
    '#title' => t('Host name/ip of SMS Gateway'),
    '#default_value' => variable_get('kannel_basic_host', 'localhost'),
    '#description' => t("Type your hostname/ip of your SMS Gateway."),
  );  
  
  $form['kannel_core'] = array(
    '#type' => 'fieldset',
    '#weight' => -90,
    '#title' => t('Core settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE    
  );
  
  $form['kannel_core']['kannel_admin_port'] = array(
    '#type' => 'textfield',
    '#title' => t('Admin Port'),
    '#default_value' => variable_get('kannel_admin_port', '13000')
  );  
  
  $form['kannel_core']['kannel_admin_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Admin Username'),
    '#default_value' => variable_get('kannel_admin_username', 'foo')
  );
  
  $form['kannel_core']['kannel_admin_password'] = array(
    '#type' => 'textfield',
    '#title' => t('Admin Password'),
    '#default_value' => variable_get('kannel_admin_password', 'bar')
  );
  
  $form['kannel_core']['kannel_tatus_password'] = array(
    '#type' => 'textfield',
    '#title' => t('Status Password'),
    '#default_value' => variable_get('kannel_status_password', 'bar')
  );
  
  $form['kannel_core']['kannel_admin_port'] = array(
    '#type' => 'textfield',
    '#title' => t('Admin Port'),
    '#default_value' => variable_get('kannel_admin_port', '13000')
  );
  
  $form['kannel_core']['kannel_smsbox_port'] = array(
    '#type' => 'textfield',
    '#title' => t('SMSBox Port'),
    '#default_value' => variable_get('kannel_smsbox_port', '13001')
  );
  
  $form['kannel_core']['kannel_wapbox_port'] = array(
    '#type' => 'textfield',
    '#title' => t('WapBox Port'),
    '#default_value' => variable_get('kannel_wapbox_port', '13002')
  );

  $form['kannel_smsbox'] = array(
    '#type' => 'fieldset',
    '#weight' => -80,
    '#title' => t('SMSBox settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE    
  );
  
  $form['kannel_smsbox']['kannel_sendsms_port'] = array(
    '#type' => 'textfield',
    '#title' => t('Send SMS Port'),
    '#default_value' => variable_get('kannel_sendsms_port', '13013')
  );  
  
  $form['kannel_sendsms'] = array(
    '#type' => 'fieldset',
    '#weight' => -70,
    '#title' => t('Send SMS User'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE    
  );
  
  $form['kannel_sendsms']['kannel_sendsms_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Send SMS Username'),
    '#default_value' => variable_get('kannel_sendsms_username', 'username')
  ); 
  
  $form['kannel_sendsms']['kannel_sendsms_password'] = array(
    '#type' => 'textfield',
    '#title' => t('Send SMS password'),
    '#default_value' => variable_get('kannel_sendsms_password', 'password')
  ); 
  
  return system_settings_form($form);
}

function kannel_admin_log() {
  $form = array();
  // only administrators can access this function
  
  // Generate the form - settings applying to all patterns first
  $form['kannel_log'] = array(
    '#type' => 'fieldset',
    '#weight' => -30,
    '#title' => t('Log settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE    
  );
  
  $form['kannel_log']['kannel_log_bearerbox'] = array(
    '#type' => 'textfield',
    '#title' => t('Path of Bearerbox log'),
    '#default_value' => variable_get('kannel_log_bearerbox', '/var/log/kannel/bearerbox.log'),
    '#description' => t("Where do you put your log file for Bearerbox"),
  );  
  
  $form['kannel_log']['kannel_log_smsbox'] = array(
    '#type' => 'textfield',
    '#title' => t('Path of Smsbox log'),
    '#default_value' => variable_get('kannel_log_bearerbox', '/var/log/kannel/smsbox.log'),
    '#description' => t("Where do you put your log file for Smsbox"),
  );
  
  return system_settings_form($form);
}

